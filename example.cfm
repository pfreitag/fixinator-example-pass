<!--- this is an example cfm file that doesn't have any vulnerabilities so it should pass the scan --->
<cfquery name="news">
    SELECT id, headline, story
    FROM news
    WHERE id = <cfqueryparam value="#url.id#">
</cfquery>

<cfoutput>
    <h1>#encodeForHTML(news.headline)#</h1>
    <p>#encodeForHTML(news.story)#</p>
</cfoutput>