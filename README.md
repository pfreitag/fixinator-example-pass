# Fixinator Bitbucket Pipeline Example #

This repo shows how a bitbucket pipeline can be setup using a `bitbucket-pipelines.yml` file.

The pipeline script runs fixinator to scan the CFML files for vulnerabilities.